<?php

namespace xtetis\xuser;

class Config extends \xtetis\xengine\models\Model
{
    /**
     * Проверяет параметры
     */
    public static function validateParams()
    {
        /*
        if (!defined('XUSER_ACTION'))
        {
            \xtetis\xengine\helpers\LogHelper::customDie('Не определена константа XUSER_ACTION');
        }
        */
    }


    /**
     * Проверяет параметры для авторизации через Google
     */
    public static function validateGoogleAuthParams()
    {
        
        if (!defined('GOOGLE_CLIENT_ID'))
        {
            \xtetis\xengine\helpers\LogHelper::customDie('Не определена константа GOOGLE_CLIENT_ID для авториации через Google');
        }
        
        
        
    }

}
