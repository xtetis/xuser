<?php

namespace xtetis\xuser;

class Component extends \xtetis\xengine\models\Component
{
    /**
     * Авторизирован ли пользователь
     */
    public static function isLoggedIn(): mixed
    {
        return \xtetis\xuser\models\UserModel::isLoggedIn();
    }

    /**
     * Возвращает блок с кнопкой для авторизации в Google
     */
    public static function getButtonGoogleAuth()
    {
        
        \xtetis\xuser\Config::validateGoogleAuthParams();

        $url_google_auth_validate = self::makeUrl([
            'path' => [
                'google_oauth',
                'google_auth_validate',
            ],
            'with_host' => true,
        ]);

        return self::renderBlock(
            'block/button_google_auth',
            [
                'url_google_auth_validate' => $url_google_auth_validate,
            ]
        );
    }
}
