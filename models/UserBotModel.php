<?php

namespace xtetis\xuser\models;

// Запрет прямого обращения
if (!defined('SYSTEM'))
{
    \xtetis\xengine\helpers\LogHelper::customDie('Не разрешен просмотр');
}

class UserBotModel extends \xtetis\xengine\models\TableModel
{
    /**
     * ID пользоваателя
     */
    public $id = 0;

    /**
     * Таблица, в которой хранятся записи о пользоваателе
     */
    public $table_name = 'xuser_bot';

    /**
     * ID пользователя
     */
    public $id_user = '';

    /**
     * Хеш, по которому можно получить доступ к аккаунту бота
     */
    public $access_hash = '';

    /**
     * Хеш актуален до
     */
    public $hash_actual = '';

    /**
     * Имя пользователя
     */
    public $name = '';

    /**
     * Логин бота
     */
    public $login = '';

    /**
     * Email бота
     */
    public $email = '';

    /**
     * Пароль бота
     */
    public $pass = '';

    /**
     * Хеш с помощью которого происходит попытка авторизации
     */
    public $auth_hash = '';

    /**
     * Возвращает связанную модель или false
     * Модели с аттрибутами связываются в массиве related_rule_list, например
     *
     * $this->related_rule_list = [
     *      'attribute_name' => \xtetis\xanyfield\models\ComponentModel::class
     * ];
     *
     * Связанная модель получается методом $this->getModelRelated('attribute_name')
     */
    public $related_rule_list = [
        'id_user' => \xtetis\xuser\models\UserModel::class,
    ];


    /**
     * Генерирует хеш, по которому можно авторизироваться под ботом
     * и обновляет поле актуальности хеша
     */
    public function generateHash()
    {
        if ($this->getErrors())
        {
            return false;
        }


        $this->access_hash = md5(uniqid());
        $this->hash_actual = date("Y-m-d H:i:s",strtotime('+1 week'));    
        $this->updateTableRecordById();
    }

    /**
     * Генерирует пользователя-бота и вставляет данные в таблицы
     */
    public function generateBot()
    {
        if ($this->getErrors())
        {
            return false;
        }

        // Генерируем логин
        $this->login = 'u' . uniqid();

        // Генерируем email
        $this->email = $this->login . '@gmail.com';

        // Генерируем пароль
        $this->pass = uniqid();

        $this->name = strval($this->name);

        if (!strlen($this->name))
        {
            $this->addError('name', 'Не указано имя бота');

            return false;
        }

        $model_user = new \xtetis\xuser\models\UserModel([
            'login'  => $this->login,
            'email'  => $this->email,
            'pass'   => md5($this->pass),
            'name'   => $this->name,
            'active' => 1,
        ]);

        $model_user->insertTableRecord();

        if ($model_user->getErrors())
        {
            $this->addError('model_user', $model_user->getLastErrorMessage());

            return false;
        }

        $this->id_user     = $model_user->id;
        $this->access_hash = md5(uniqid());
        $this->hash_actual = date("Y-m-d H:i:s",strtotime('+1 week'));    

        $this->insertTableRecord();

        if ($this->getErrors())
        {
            return false;
        }

        return true;
    }

    /**
     * Возвращает список моделей ботов
     */
    public static function getBotModelList($params = [
        'offset' => 0,
        'limit'  => 20,
    ])
    {
        if (!isset($params['offset']))
        {
            $params['offset'] = 0;
        }

        if (!isset($params['limit']))
        {
            $params['limit'] = 20;
        }

        $sql = '
        SELECT id
        FROM xuser_bot
        ORDER BY id DESC
        LIMIT ' . intval($params['limit']) . '
        OFFSET ' . intval($params['offset']) . '

        ';

        return self::getModelListBySql($sql);
    }

    /**
     * Возвращает ссылку для авторизации бота
     */
    public function getBotLoginLink()
    {
        if ($this->getErrors())
        {
            return false;
        }

        return \xtetis\xuser\Component::makeUrl([
            'path'  => [
                'bauth',
                'index',
            ],
            'query' => [
                'id'   => $this->id,
                'hash' => $this->access_hash,
            ],
            'with_host' => true,
        ]);
    }



    /**
     * Авторизация по хешу в аккаунт бота
     */
    public function loginBot()
    {
        if ($this->getErrors())
        {
            return false;
        }

        // Если хеш уже просрочен
        if (strtotime('now')>strtotime($this->hash_actual))
        {
            $this->addError('hash_actual', 'Хеш просрочен');

            return false;
        }

        date("Y-m-d H:i:s",strtotime('+1 week'));    

        if ($this->auth_hash !== $this->access_hash)
        {
            $this->addError('auth_hash', 'Неверный хеш, хеш будет сброшен');
            $this->generateHash();

            return false;
        }

        $this->generateHash();

        $model_user = \xtetis\xuser\models\UserModel::generateModelById($this->id_user);
        if (!$model_user)
        {
            $this->addError('auth_hash', 'Не найден пользователь');

            return false;
        }

        // Авторизируем пользователя
        $model_user->saveModel();


        // Обновляет в БД дату последнего доступа
        $model_user->updateLastActivity();
        
        return true;
    }

}
