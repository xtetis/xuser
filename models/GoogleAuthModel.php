<?php

namespace xtetis\xuser\models;

// Запрет прямого обращения
if (!defined('SYSTEM'))
{
    \xtetis\xengine\helpers\LogHelper::customDie('Не разрешен просмотр');
}

class GoogleAuthModel extends \xtetis\xengine\models\TableModel
{
    /**
     * ID пользоваателя
     */
    public $id = 0;

    /**
     * Таблица, в которой хранятся записи о пользоваателе
     */
    public $table_name = 'xuser_google_auth';

    /**
     * Email пользователя Google
     */
    public $email = '';

    /**
     * Имя+Фамилия
     */
    public $name = '';

    /**
     * Имя
     */
    public $given_name = '';

    /**
     * Фамилия
     */
    public $family_name = '';

    /**
     * Локаль
     */
    public $locale = '';

    /**
     * Лого пользователя
     */
    public $picture = '';

    /**
     * Добавляет данные об авторизации через Google
     */
    public function addGoogleAuthInfo()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->email       = strval($this->email);
        $this->name        = strval($this->name);
        $this->given_name  = strval($this->given_name);
        $this->family_name = strval($this->family_name);
        $this->locale      = strval($this->locale);
        $this->picture     = strval($this->picture);

        if (!$this->insertTableRecord())
        {
            return false;
        }

        return true;
    }
}
