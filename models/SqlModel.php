<?php


namespace xtetis\xuser\models;


// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

/**
 *
 */
class SqlModel
{

    /**
     * Авторизация пользователя
     */
    public static function xuserLoginUser(
        $email = '',
        $password = ''
    )
    {
        $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect(DEFAULT_DB);
        $stmt    = $connect->prepare('CALL xuser_login_user(:email,:pass,@result,@result_str)');

        $stmt->bindParam('email', $email, \PDO::PARAM_STR | \PDO::PARAM_INPUT_OUTPUT, 100);
        $stmt->bindParam('pass', $password, \PDO::PARAM_STR | \PDO::PARAM_INPUT_OUTPUT, 100);

        $stmt->execute();

        $stmt    = $connect->prepare('SELECT @result as result, @result_str as result_str; ');
        
        $stmt->execute();

        $row = $stmt->fetch();

        $ret['result'] = $row['result'];
        $ret['result_str'] = $row['result_str'];

        return $ret;
    }

    /**
     * Изменение пароля пользователя по токену
     * Токен одноразовый
     */
    public static function xuserChangePassByToken(
        $token = '',
        $pass = ''
    )
    {
        $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect(DEFAULT_DB);
        $stmt    = $connect->prepare('CALL xuser_change_pass_by_token(:token,:pass,@result,@result_str)');

        $stmt->bindParam('token', $token, \PDO::PARAM_STR | \PDO::PARAM_INPUT_OUTPUT, 100);
        $stmt->bindParam('pass', $pass, \PDO::PARAM_STR | \PDO::PARAM_INPUT_OUTPUT, 100);

        $stmt->execute();

        $stmt    = $connect->prepare('SELECT @result as result, @result_str as result_str; ');
        
        $stmt->execute();

        $row = $stmt->fetch();

        $ret['result'] = $row['result'];
        $ret['result_str'] = $row['result_str'];

        return $ret;
    }

    /**
     * Активация пользователя
     */
    public static function xuserActivateUser(
        $id = 0,
        $token = ''
    )
    {
        $id = intval($id);

        $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect(DEFAULT_DB);
        $stmt    = $connect->prepare('CALL xuser_activate_user(:id,:token,@result,@result_str)');

        $stmt->bindParam('id', $id, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT);
        $stmt->bindParam('token', $token, \PDO::PARAM_STR | \PDO::PARAM_INPUT_OUTPUT, 100);

        $stmt->execute();

        $stmt    = $connect->prepare('SELECT @result as result, @result_str as result_str; ');
        
        $stmt->execute();

        $row = $stmt->fetch();

        $ret['result'] = $row['result'];
        $ret['result_str'] = $row['result_str'];

        return $ret;
    }


    

    /**
     * Проверка существования указанного логина
     */
    public static function xuserIsLoginExists(
        $login = ''
    )
    {
        $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect(DEFAULT_DB);
        $stmt    = $connect->prepare('CALL xuser_is_login_exists(:login,@result,@result_str)');

        $stmt->bindParam('login', $login, \PDO::PARAM_STR | \PDO::PARAM_INPUT_OUTPUT, 100);

        $stmt->execute();

        $stmt    = $connect->prepare('SELECT @result as result, @result_str as result_str; ');
        
        $stmt->execute();

        $row = $stmt->fetch();

        $ret['result'] = $row['result'];
        $ret['result_str'] = $row['result_str'];

        return $ret;
    }

    /**
     * Проверка существования указанного Email
     */
    public static function xuserIsEmailRegistered(
        $email = ''
    )
    {
        $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect(DEFAULT_DB);
        $stmt    = $connect->prepare('CALL xuser_is_email_registered(:email,@result,@result_str)');

        $stmt->bindParam('email', $email, \PDO::PARAM_STR | \PDO::PARAM_INPUT_OUTPUT, 100);

        $stmt->execute();

        $stmt    = $connect->prepare('SELECT @result as result, @result_str as result_str; ');
        
        $stmt->execute();

        $row = $stmt->fetch();

        $ret['result'] = $row['result'];
        $ret['result_str'] = $row['result_str'];


        return $ret;
    }



    /**
     * Регистрация пользователя
     */
    public static function xuserAddUser(
        $login = '',
        $email = '',
        $password = ''
    )
    {
        $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect(DEFAULT_DB);
        $stmt    = $connect->prepare('CALL xuser_add_user(:login, :email,:pass,@result,@result_str)');

        $stmt->bindParam('login', $login, \PDO::PARAM_STR | \PDO::PARAM_INPUT_OUTPUT, 100);
        $stmt->bindParam('email', $email, \PDO::PARAM_STR | \PDO::PARAM_INPUT_OUTPUT, 100);
        $stmt->bindParam('pass', $password, \PDO::PARAM_STR | \PDO::PARAM_INPUT_OUTPUT, 100);

        $stmt->execute();

        $stmt    = $connect->prepare('SELECT @result as result, @result_str as result_str; ');
        
        $stmt->execute();

        $row = $stmt->fetch();

        $ret['result'] = $row['result'];
        $ret['result_str'] = $row['result_str'];

        return $ret;
    }





    /**
     * Возвращает токен для активации пользователя
     */
    public static function xuserGetUserActivateToken(
        $id = 0
    )
    {
        $id = intval($id);


        $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect(DEFAULT_DB);
        $stmt    = $connect->prepare('CALL xuser_get_user_activate_token(:id,@result,@result_str)');

        $stmt->bindParam('id', $id, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT);

        $stmt->execute();

        $stmt    = $connect->prepare('SELECT @result as result, @result_str as result_str; ');
        
        $stmt->execute();

        $row = $stmt->fetch();

        $ret['result'] = $row['result'];
        $ret['result_str'] = $row['result_str'];


        return $ret;
    }





    /**
     * Возвращает токен для восстановления пароля
     */
    public static function xuserGetUserRecoveryPassToken(
        $id = 0
    )
    {
        $id = intval($id);


        $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect(DEFAULT_DB);
        $stmt    = $connect->prepare('CALL xuser_get_user_recovery_pass_token(:id,@result,@result_str)');

        $stmt->bindParam('id', $id, \PDO::PARAM_INT | \PDO::PARAM_INPUT_OUTPUT);

        $stmt->execute();

        $stmt    = $connect->prepare('SELECT @result as result, @result_str as result_str; ');
        
        $stmt->execute();

        $row = $stmt->fetch();

        $ret['result'] = $row['result'];
        $ret['result_str'] = $row['result_str'];


        return $ret;
    }


    

    /**
     * Проверка Email на активированность
     */
    public static function xuserIsUserActivated(
        $email = ''
    )
    {
        $connect = \xtetis\xengine\helpers\DbConnectHelper::getConnect(DEFAULT_DB);
        $stmt    = $connect->prepare('CALL xuser_is_user_activated(:email,@result,@result_str)');

        $stmt->bindParam('email', $email, \PDO::PARAM_STR | \PDO::PARAM_INPUT_OUTPUT, 100);

        $stmt->execute();

        $stmt    = $connect->prepare('SELECT @result as result, @result_str as result_str; ');
        
        $stmt->execute();

        $row = $stmt->fetch();

        $ret['result'] = $row['result'];
        $ret['result_str'] = $row['result_str'];


        return $ret;
    }


}
