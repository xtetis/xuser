<?php

namespace xtetis\xuser\models;

// Запрет прямого обращения
if (!defined('SYSTEM'))
{
    \xtetis\xengine\helpers\LogHelper::customDie('Не разрешен просмотр');
}

 
use Gravatar\Image;
use Gravatar\Profile;

class GravatarModel extends \xtetis\xengine\models\TableModel
{
    /**
     * Возвращает картинку Gravatar
     */
    public static function getGravatar(?string $email = null): Image
    {
        return new Image($email);
    }

    /**
     * Возвращает ссылку на Gravatar
     */
    public static function getGravatarProfile(?string $email = null): Profile
    {
        return new Profile($email);
    }

}
