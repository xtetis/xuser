<?php

namespace xtetis\xuser\models;

// Запрет прямого обращения
if (!defined('SYSTEM'))
{
    \xtetis\xengine\helpers\LogHelper::customDie('Не разрешен просмотр');
}

class UserModel extends \xtetis\xengine\models\TableModel
{
    /**
     * ID пользоваателя
     */
    public $id = 0;

    /**
     * @var string
     */
    public $email = '';
    /**
     * @var string
     */
    public $pass = '';
    /**
     * @var string
     */
    public $captcha = '';

    /**
     * Имя пользователя (не логин)
     */
    public $name = '';
    /**
     * @var string
     */
    public $login = '';
    /**
     * @var string
     */
    public $pass_repeat = '';

    /**
     * @var array
     */
    public $login_user_result = [];

    /**
     * @var array
     */
    public $register_user_result = [];

    /**
     * Зарегистрировал ли пользователь с таким логином
     */
    public $is_login_exists_result = [];

    /**
     * @var array
     */
    public $is_email_registered_result = [];

    /**
     * @var array
     */
    public $get_user_activate_token_result = [];

    /**
     * Результат получения токена для пользоватетя для
     * последующего восстановления пароля
     */
    public $get_user_recovery_pass_token_result = [];

    /**
     * @var array
     */
    public $is_user_activated_result = [];

    /**
     * Токен
     */
    public $token = '';

    /**
     * Результат активации пользователя по токену
     */
    public $activate_user_result = [];

    /**
     * Результат изменения пароля по токену
     */
    public $change_pass_by_token_result = [];

    /**
     * Таблица, в которой хранятся записи о пользоваателе
     */
    public $table_name = 'xuser_user';

    /**
     * Информауция о пользователе от Google после авторизации через Google OAuth
     */
    public $user_google = [];

    /**
     * Активирована ли учетная запись
     */
    public $active = 0;

    /**
     * Дата время последнего доступа
     */
    public $last_access = 'NOW';

    /**
     * Активирует пользователя
     */
    public function activateAccount($params = [])
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->id = intval($this->id);

        if (!$this->id)
        {
            $this->addError('common', 'Не заполнен ID пользователя');

            return false;
        }

        if (!strlen($this->token))
        {
            $this->addError('token', 'Не заполнен токен');

            return false;
        }

        if (strlen($this->token) > 99)
        {
            $this->addError('token', 'Максимальная длина токена 99 символов');

            return false;
        }

        $this->activate_user_result = \xtetis\xuser\models\SqlModel::xuserActivateUser(
            $this->id,
            $this->token
        );

        if ($this->activate_user_result['result'] < 0)
        {
            $this->addError('common', $this->activate_user_result['result_str']);

            return false;
        }

        return true;
    }

    /**
     * Получает результат авторизации в переменную login_user_result
     */
    public function setLoginUseresult()
    {
        if ($this->getErrors())
        {
            return false;
        }

        // Попытка авторизации с помощью email и пароля
        $this->login_user_result = \xtetis\xuser\models\SqlModel::xuserLoginUser(
            strval($this->email),
            strval($this->pass)
        );

        if ($this->login_user_result['result'] < 0)
        {
            return false;
        }

        $this->id = $this->login_user_result['result'];

        $this->getById();

        // Обновляет в БД дату последнего доступа
        $this->updateLastActivity();

        return true;
    }

    /**
     * @param array $params
     */
    public function loginByEmailPass($params = [])
    {
        if ($this->getErrors())
        {
            return false;
        }

        if (!strlen($this->email))
        {
            $this->addError('email', 'Не заполнен Email');

            return false;
        }

        if (strlen($this->email) > 99)
        {
            $this->addError('email', 'Максимальная длина Email 99 символов');

            return false;
        }

        if (!filter_var($this->email, FILTER_VALIDATE_EMAIL))
        {
            $this->addError('email', 'Указанный Email некорректный');

            return false;
        }

        if (!strlen($this->pass))
        {
            $this->addError('pass', 'Не заполнен пароль');

            return false;
        }

        if (strlen($this->pass) > 99)
        {
            $this->addError('pass', 'Максимальная длина пароля 99 символов');

            return false;
        }

        if (!strlen($this->captcha))
        {
            $this->addError('captcha', 'Не заполнена капча');

            return false;
        }

        // Проверка капчи
        if (!\xtetis\xcaptcha\Component::checkCaptcha($this->captcha))
        {
            $this->addError('captcha', 'Вы неверно прошли капчу');

            return false;
        }

        // Проверка существования пользователя с таким Email
        $this->is_email_registered_result = \xtetis\xuser\models\SqlModel::xuserIsEmailRegistered(
            $this->email
        );

        // Если пользователь с таким Email не зарегистрирован
        if ($this->is_email_registered_result['result'] < 0)
        {
            $this->addError('email', $this->is_email_registered_result['result_str']);

            return false;
        }

        // ПРоверка - активирован ли пользователь
        $this->is_user_activated_result = \xtetis\xuser\models\SqlModel::xuserIsUserActivated(
            $this->email
        );

        // Если не активирован
        if ($this->is_user_activated_result['result'] < 0)
        {
            $resend_activate_link = \xtetis\xuser\Component::makeUrl([
                'path' => [
                    'activate_account',
                    'resend_activate',
                ],
            ]);

            $message = 'Учетная запись не активирована, активируйте её по ссылке на почте или <a href="' .
                $resend_activate_link .
                '">повторно отправьте письмо</a> для активации';

            $this->addError('email', $message);

            return false;
        }

        // Получает результат авторизации в переменную login_user_result
        $this->setLoginUseresult();

        if ($this->getErrors())
        {
            return false;
        }

        if ($this->login_user_result['result'] < 0)
        {
            $this->addError('pass', $this->login_user_result['result_str']);

            return false;
        }

        // Обнуляем хеш пароля, чтобы в случае доступа к сессии он был
        // недоступен для злоумышленников
        $this->pass = '';
        $this->result_sql = [];

        $this->saveModel();

        

        // Обновляет в БД дату последнего доступа
        $this->updateLastActivity();

        return true;
    }

    /**
     * Отправка Email для восстановления пароля
     */
    public function sendRecoveryPass($params = [])
    {
        if ($this->getErrors())
        {
            return false;
        }

        if (!strlen($this->email))
        {
            $this->addError('email', 'Не заполнен Email');

            return false;
        }

        if (strlen($this->email) > 99)
        {
            $this->addError('email', 'Максимальная длина Email 99 символов');

            return false;
        }

        if (!filter_var($this->email, FILTER_VALIDATE_EMAIL))
        {
            $this->addError('email', 'Указанный Email некорректный');

            return false;
        }

        if (!strlen($this->captcha))
        {
            $this->addError('captcha', 'Не заполнена капча');

            return false;
        }

        // Проверка капчи
        if (!\xtetis\xcaptcha\Component::checkCaptcha($this->captcha))
        {
            $this->addError('captcha', 'Вы неверно прошли капчу');

            return false;
        }

        // Проверка существования пользователя с таким Email
        $this->is_email_registered_result = \xtetis\xuser\models\SqlModel::xuserIsEmailRegistered(
            $this->email
        );

        // Если пользователь с таким Email не зарегистрирован
        if ($this->is_email_registered_result['result'] < 0)
        {
            $this->addError('email', $this->is_email_registered_result['result_str']);

            return false;
        }

        $this->id    = $this->is_email_registered_result['result'];
        $this->login = $this->is_email_registered_result['result_str'];

        // ПРоверка - активирован ли пользователь
        $this->is_user_activated_result = \xtetis\xuser\models\SqlModel::xuserIsUserActivated(
            $this->email
        );

        // Если не активирован
        if ($this->is_user_activated_result['result'] < 0)
        {

            $url_resend_activate = \xtetis\xuser\Component::makeUrl([
                'path' => [
                    'resend_activate',
                ],
            ]);

            $message = 'Учетная запись не активирована, активируйте её по ссылке, отправленной ранее на ваш Email или <a href="' .
                $url_resend_activate .
                '">повторно отправьте письмо</a> для активации';

            $this->addError('email', $message);

            return false;
        }

        // Результат генерации токена для восстановления пароля
        $this->get_user_recovery_pass_token_result = \xtetis\xuser\models\SqlModel::xuserGetUserRecoveryPassToken(
            $this->id
        );

        // Если токен успешно сгенерирован
        if ($this->get_user_recovery_pass_token_result['result'] < 0)
        {
            $this->addError('common', $this->get_user_recovery_pass_token_result['result_str']);

            return false;
        }

        $this->token = $this->get_user_recovery_pass_token_result['result_str'];

        // Отправляем Email со ссылкой для восстановлшения пароля
        $this->sendRecoveryPassEmail();

        // Сохраняем в сессии сообщение для отображения на информационной странице
        $message = 'На ваш email ' . $this->email .
            ' отправлено письмо со ссылкой для изменения пароля. Пожалуйста, проверьте вашу почту и ' .
            ' перейдите по указанной в письме ссылке';

        // Устанавливаем параметры для отображения страницы с сообщением для пользователя с единым урлом
        \xtetis\xpagemessage\Component::setPageMessageParams([
            'message' => $message,
            'name'    => 'Восстановление пароля',
            'title'   => 'Вам отправлено письмо',
        ]);

        return true;
    }

    /**
     * Повторная отправка Email для активации аккаунта
     */
    public function sendUserActivateEmail($params = [])
    {
        if ($this->getErrors())
        {
            return false;
        }

        if (!strlen($this->email))
        {
            $this->addError('email', 'Не заполнен Email');

            return false;
        }

        if (strlen($this->email) > 99)
        {
            $this->addError('email', 'Максимальная длина Email 99 символов');

            return false;
        }

        if (!filter_var($this->email, FILTER_VALIDATE_EMAIL))
        {
            $this->addError('email', 'Указанный Email некорректный');

            return false;
        }

        if (!strlen($this->captcha))
        {
            $this->addError('captcha', 'Не заполнена капча');

            return false;
        }

        // Проверка капчи
        if (!\xtetis\xcaptcha\Component::checkCaptcha($this->captcha))
        {
            $this->addError('captcha', 'Вы неверно прошли капчу');

            return false;
        }

        // Проверка существования пользователя с таким Email
        $this->is_email_registered_result = \xtetis\xuser\models\SqlModel::xuserIsEmailRegistered(
            $this->email
        );

        // Если пользователь с таким Email не зарегистрирован
        if ($this->is_email_registered_result['result'] < 0)
        {
            $this->addError('email', $this->is_email_registered_result['result_str']);

            return false;
        }

        $this->id    = $this->is_email_registered_result['result'];
        $this->login = $this->is_email_registered_result['result_str'];

        // ПРоверка - активирован ли пользователь
        $this->is_user_activated_result = \xtetis\xuser\models\SqlModel::xuserIsUserActivated(
            $this->email
        );

        // Если активирован
        if ($this->is_user_activated_result['result'] > 0)
        {

            $this->addError('email', 'Учетная запись уже активирована');

            return false;
        }

        $this->get_user_activate_token_result = \xtetis\xuser\models\SqlModel::xuserGetUserActivateToken(
            $this->id
        );

        if ($this->get_user_activate_token_result['result'] < 0)
        {
            $this->addError('common', $this->get_user_activate_token_result['result_str']);

            return false;
        }

        $this->token = $this->get_user_activate_token_result['result_str'];

        // Отправляем Email о регистрации с подтверждением
        $this->sendRegisterEmail();

        // Сохраняем в сессии сообщение для отображения на информационной странице
        $message = $this->login . ', мы отправили на ваш email ' . $this->email .
            ' письмо с подтверждением. Пожалуйста, проверьте вашу почту и ' .
            ' перейдите по указанной в письме ссылке для активации аккаунта';

        // Устанавливаем параметры для отображения страницы с сообщением для пользователя с единым урлом
        \xtetis\xpagemessage\Component::setPageMessageParams([
            'message' => $message,
            'name'    => 'Письмо с подтверждением регистрации',
            'title'   => 'Вам отправлено письмо',
        ]);

        return true;
    }

    /**
     * Регистрация пользователя
     */
    public function registerUser()
    {
        if ($this->getErrors())
        {
            return false;
        }

        if (!mb_strlen($this->login))
        {
            $this->addError('login', 'Не заполнено имя пользователя');

            return false;
        }

        if (mb_strlen($this->login) > 25)
        {
            $this->addError('login', 'Максимальная длина логина 25 символов');

            return false;
        }

        if (mb_strlen($this->login) < 3)
        {
            $this->addError('login', 'Минимальная длина логина 3 символа');

            return false;
        }

        if (!strlen($this->email))
        {
            $this->addError('email', 'Не заполнен Email');

            return false;
        }

        if (strlen($this->email) > 99)
        {
            $this->addError('email', 'Максимальная длина Email 99 символов');

            return false;
        }

        $this->pass = trim($this->pass);

        if (!strlen($this->pass))
        {
            $this->addError('pass', 'Не заполнен пароль');

            return false;
        }

        if (strlen($this->pass) > 99)
        {
            $this->addError('pass', 'Максимальная длина пароля 99 символов');

            return false;
        }

        if (!strlen($this->pass_repeat))
        {
            $this->addError('pass_repeat', 'Не заполнено поле');

            return false;
        }

        if (!strlen($this->captcha))
        {
            $this->addError('captcha', 'Не заполнена капча');

            return false;
        }

        if (strlen($this->pass) < 6)
        {
            $this->addError('pass', 'Минимальная длина пароля 6 символов');

            return false;
        }

        if ($this->pass != $this->pass_repeat)
        {
            $this->addError('pass_repeat', 'Указанные пароли не совпадают');

            return false;
        }

        if (!filter_var($this->email, FILTER_VALIDATE_EMAIL))
        {
            $this->addError('email', 'Указанный Email некорректный');

            return false;
        }

        // Проверка капчи
        if (!\xtetis\xcaptcha\Component::checkCaptcha($this->captcha))
        {
            $this->addError('captcha', 'Вы неверно прошли капчу');

            return false;
        }

        // Зарегистрировал ли пользователь с таким логином
        $this->is_login_exists_result = \xtetis\xuser\models\SqlModel::xuserIsLoginExists(
            $this->login
        );

        // Если уже есть пользователь с таким логином
        if ($this->is_login_exists_result['result'] > 0)
        {
            $this->addError('login', 'Пользователь с таким логином уже зарегистрирован');

            return false;
        }

        // Проверка существования пользователя с таким Email
        $this->is_email_registered_result = \xtetis\xuser\models\SqlModel::xuserIsEmailRegistered(
            $this->email
        );

        // Если пользователь с таким Email уже зарегистрирован
        if ($this->is_email_registered_result['result'] > 0)
        {
            $this->addError('email', 'Пользователь с таким Email уже зарегистрирован');

            return false;
        }

        $this->register_user_result = \xtetis\xuser\models\SqlModel::xuserAddUser(
            $this->login,
            $this->email,
            $this->pass
        );

        if ($this->register_user_result['result'] < 0)
        {
            $this->addError('common', $this->register_user_result['result_str']);

            return false;
        }

        $this->id = $this->register_user_result['result'];

        $this->get_user_activate_token_result = \xtetis\xuser\models\SqlModel::xuserGetUserActivateToken(
            $this->id
        );

        if ($this->get_user_activate_token_result['result'] < 0)
        {
            $this->addError('common', $this->get_user_activate_token_result['result_str']);

            return false;
        }

        $this->token = $this->get_user_activate_token_result['result_str'];

        // Отправляем Email о регистрации с подтверждением
        $this->sendRegisterEmail();

        // Сохраняем в сессии сообщение для отображения на информационной странице
        $message = $this->login . ', мы отправили на ваш email ' . $this->email .
            ' письмо с подтверждением. Пожалуйста, проверьте вашу почту и ' .
            ' перейдите по указанной в письме ссылке для активации аккаунта';

        // Устанавливаем параметры для отображения страницы с сообщением для пользователя с единым урлом
        \xtetis\xpagemessage\Component::setPageMessageParams([
            'message' => $message,
            'name'    => 'Письмо с подтверждением',
            'title'   => 'Вам отправлено письмо',
        ]);

        return true;
    }

    /**
     * Изменение пароля пользователя по токену
     * (для неавторизированных пользователей)
     */
    public function changeUserPassByToken()
    {
        if ($this->getErrors())
        {
            return false;
        }

        if (!mb_strlen($this->token))
        {
            $this->addError('common', 'Не заполнен токен');

            return false;
        }

        if (mb_strlen($this->token) > 50)
        {
            $this->addError('common', 'Максимальная длина токена 50 символов');

            return false;
        }

        $this->pass = trim($this->pass);

        if (!strlen($this->pass))
        {
            $this->addError('pass', 'Не заполнен пароль');

            return false;
        }

        if (strlen($this->pass) > 99)
        {
            $this->addError('pass', 'Максимальная длина пароля 99 символов');

            return false;
        }

        if (!strlen($this->pass_repeat))
        {
            $this->addError('pass_repeat', 'Не заполнено поле');

            return false;
        }

        if (!strlen($this->captcha))
        {
            $this->addError('captcha', 'Не заполнена капча');

            return false;
        }

        if (strlen($this->pass) < 6)
        {
            $this->addError('pass', 'Минимальная длина пароля 6 символов');

            return false;
        }

        if ($this->pass != $this->pass_repeat)
        {
            $this->addError('pass_repeat', 'Указанные пароли не совпадают');

            return false;
        }

        // Проверка капчи
        if (!\xtetis\xcaptcha\Component::checkCaptcha($this->captcha))
        {
            $this->addError('captcha', 'Вы неверно прошли капчу');

            return false;
        }

        // Изменение пароля пользователя по токену
        $this->change_pass_by_token_result = \xtetis\xuser\models\SqlModel::xuserChangePassByToken(
            $this->token,
            $this->pass
        );

        // Если изменение пароля успешно прошло
        if ($this->change_pass_by_token_result['result'] < 0)
        {
            $this->addError('common', $this->change_pass_by_token_result['result_str']);

            return false;
        }

        $this->id    = $this->change_pass_by_token_result['result'];
        $this->email = $this->change_pass_by_token_result['result_str'];

        // Отправляем Email о смене пароля
        $this->sendChangePassEmail();

        // Сохраняем в сессии сообщение для отображения на информационной странице
        $message = 'Ваш пароль успешно изменен на <b>' . $this->pass . '</b>. ' .
        'На ваш email ' . $this->email . ' отправлено письмо с новым паролем';

        // Устанавливаем параметры для отображения страницы с сообщением для пользователя с единым урлом
        \xtetis\xpagemessage\Component::setPageMessageParams([
            'message' => $message,
            'name'    => 'Пароль изменен',
            'title'   => 'Вам отправлено письмо',
        ]);

        return true;
    }

    /**
     * Отправка Email о регистрации с подтверждением
     */
    public function sendRegisterEmail()
    {

        //Create a new PHPMailer instance
        $mail = new \PHPMailer\PHPMailer\PHPMailer();
        //Set PHPMailer to use the sendmail transport
        $mail->isSendmail();
        $mail->Timeout = 10;
        //Set who the message is to be sent from
        $mail->setFrom('noreply@' . $_SERVER['HTTP_HOST'], APP_NAME);

        // Кому
        $mail->addAddress($this->email);
        //Set the subject line
        $mail->Subject = 'Регистрация на ' . APP_NAME;

        // Урл главной страницы с хостом
        $url_site_main_w_host = \xtetis\xengine\helpers\UrlHelper::makeUrl(['with_host' => true]);

        // Урл активации аккаунта
        $url_activate_account = \xtetis\xuser\Component::makeUrl([
            'path'      => [
                'activate_account',
            ],
            'query'     => [
                'token' => $this->token,
                'id'    => $this->id,
            ],
            'with_host' => true,
        ]);

        $params = [
            'id'                   => $this->id,
            'login'                => $this->login,
            'email'                => $this->email,
            'token'                => $this->token,
            'url_site_main_w_host' => $url_site_main_w_host,
            'url_activate_account' => $url_activate_account,
        ];

        // Возвращает отрендеренное тело письма о регистрации
        $email_body = \xtetis\xuser\Component::renderBlock(
            'email/email_register',
            $params
        );

        $mail->msgHTML($email_body);

        if (!$mail->send())
        {
            $this->addError('common', 'Mailer Error: ' . $mail->ErrorInfo);

            return false;
        }

        $email_full_message = $mail->getSentMIMEMessage();
        file_put_contents(LOG_DIRECTORY . '/last_email.eml', $email_full_message);

        return true;
    }

    /**
     * Отправка Email о регистрации без подтверждения (через Google OAuth)
     */
    public function sendRegisterGoogleEmail()
    {

        //Create a new PHPMailer instance
        $mail = new \PHPMailer\PHPMailer\PHPMailer();
        //Set PHPMailer to use the sendmail transport
        $mail->isSendmail();
        $mail->CharSet  = 'UTF-8';
        $mail->Encoding = 'base64';
        $mail->Timeout  = 10;
        //Set who the message is to be sent from
        $mail->setFrom('noreply@' . $_SERVER['HTTP_HOST'], APP_NAME);

        // Кому
        $mail->addAddress($this->email);
        //Set the subject line
        $mail->Subject = 'Регистрация на ' . APP_NAME;

        // Урл главной страницы с хостом
        $url_site_main_w_host = \xtetis\xengine\helpers\UrlHelper::makeUrl(['with_host' => true]);

        $params = [
            'id'                   => $this->id,
            'login'                => $this->login,
            'email'                => $this->email,
            'pass'                 => $this->pass,
            'url_site_main_w_host' => $url_site_main_w_host,
        ];

        // Возвращает отрендеренное тело письма о регистрации
        $email_body = \xtetis\xuser\Component::renderBlock(
            'email/email_register_google',
            $params
        );

        $mail->msgHTML($email_body);

        if (!$mail->send())
        {
            $this->addError('common', 'Mailer Error: ' . $mail->ErrorInfo);

            return false;
        }

        $email_full_message = $mail->getSentMIMEMessage();
        @file_put_contents(LOG_DIRECTORY . '/last_email.eml', $email_full_message);

        return true;
    }

    /**
     * Отправка Email со ссылкой для восстановления пароля
     */
    public function sendRecoveryPassEmail()
    {

        //Create a new PHPMailer instance
        $mail = new \PHPMailer\PHPMailer\PHPMailer();
        //Set PHPMailer to use the sendmail transport
        $mail->isSendmail();
        $mail->Timeout = 10;
        //Set who the message is to be sent from
        $mail->setFrom('noreply@' . $_SERVER['HTTP_HOST'], APP_NAME);

        // Кому
        $mail->addAddress($this->email);

        $mail->Subject = 'Восстановление пароля на ' . APP_NAME;

        // Урл восстановления пароля по токену
        $url_recovery_pass = \xtetis\xuser\Component::makeUrl([
            'path'      => [
                'change_pass',
                'by_token',
            ],
            'query'     => [
                'token' => $this->token,
            ],
            'with_host' => true,
        ]);

        $params = [
            'url_recovery_pass' => $url_recovery_pass,
        ];

        // Возвращает отрендеренное тело письма для восстановления пароля
        $recovery_pass_body = \xtetis\xuser\Component::renderBlock(
            'email/email_recovery_pass',
            $params
        );

        $mail->msgHTML($recovery_pass_body);

        if (!$mail->send())
        {
            $this->addError('common', 'Mailer Error: ' . $mail->ErrorInfo);

            return false;
        }

        $email_full_message = $mail->getSentMIMEMessage();
        file_put_contents(LOG_DIRECTORY . '/last_email.eml', $email_full_message);

        return true;
    }

    /**
     * Отправка Email об изменении пароля
     */
    public function sendChangePassEmail()
    {

        //Create a new PHPMailer instance
        $mail = new \PHPMailer\PHPMailer\PHPMailer();
        //Set PHPMailer to use the sendmail transport
        $mail->isSendmail();
        $mail->Timeout = 10;

        //Set who the message is to be sent from
        $mail->setFrom('noreply@' . $_SERVER['HTTP_HOST'], APP_NAME);

        // Кому
        $mail->addAddress($this->email);

        $mail->Subject = 'Изменение пароля на ' . APP_NAME;

        $main_url_w_host = \xtetis\xengine\helpers\UrlHelper::makeUrl(['with_host' => true]);

        $params = [
            'pass'            => $this->pass,
            'main_url_w_host' => $main_url_w_host,
        ];

        // Возвращает отрендеренное тело письма для восстановления пароля
        $change_pass_body = \xtetis\xuser\Component::renderBlock(
            'email/email_change_pass',
            $params
        );

        $mail->msgHTML($change_pass_body);

        if (!$mail->send())
        {
            $this->addError('common', 'Mailer Error: ' . $mail->ErrorInfo);

            return false;
        }

        $email_full_message = $mail->getSentMIMEMessage();
        file_put_contents(LOG_DIRECTORY . '/last_email.eml', $email_full_message);

        return true;
    }

    /**
     * @param $string
     */
    public static function isLoggedIn()
    {
        $model = self::getModel(
            '',
            [],
            false
        );

        if (!$model)
        {
            return false;
        }

        return $model;
    }

    /**
     * Возвращает ID пользователя или 0 если пользователь не авторизирован
     */
    public static function getIdUser()
    {
        $model = self::isLoggedIn();
        if (!$model)
        {
            return 0;
        }
        else
        {
            return intval($model->id);
        }
    }

    /**
     * @param $string
     */
    public static function logout()
    {
        self::flushModel('');
    }

    /**
     * Возвращает модель пользователя по ID
     */
    public function getById()
    {
        if ($this->getErrors())
        {
            return false;
        }

        if (!$this->getModelById())
        {
            return false;
        }

        return true;
    }

    /**
     * Возвращает имя пользователя
     *
     * @return mixed
     */
    public function getUserLoginOrName()
    {
        if ($this->getErrors())
        {
            return false;
        }

        if (!strlen(strval($this->name)))
        {
            return $this->login;
        }

        return $this->name;

    }

    /**
     * Обновляет имя пользователя
     */
    public function updateName()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->name = strval($this->name);

        if (!strlen($this->name))
        {
            $this->addError('name', 'Не указано имя');

            return false;
        }

        if (strip_tags($this->name) != $this->name)
        {
            $this->addError('name', 'Не допускаются HTML теги');

            return false;
        }

        mb_regex_encoding('UTF-8');
        $tmp_name = preg_replace('/([^\w\ \-\+]+)/iu', '', $this->name);
        if ($tmp_name != $this->name)
        {
            $this->addError('name', 'В имени не допускаются спецсимволы');

            return false;
        }

        $this->name = trim($this->name);

        if (mb_strlen($this->name) < 2)
        {
            $this->addError('name', 'Минимальная длина имени 2 символа');

            return false;
        }

        if (mb_strlen($this->name) > 30)
        {
            $this->addError('name', 'Максимальная длина имени 30 символов');

            return false;
        }

        // Список полей, кооторые обновить
        $this->insert_update_field_list = ['name'];

        if (!$this->updateTableRecordById())
        {
            return false;
        }

        // Обнуляем хеш пароля, чтобы в случае доступа к сессии он был
        // недоступен для злоумышленников
        $this->pass = '';
        $this->result_sql = [];

        // Обновляет в сессии данные о модели пользователя
        $this->saveModel();

        

        // Обновляет в БД дату последнего доступа
        $this->updateLastActivity();

        return true;
    }

    /**
     * @param array $params
     */
    public function loginByGoogleOauth($params = [])
    {
        if ($this->getErrors())
        {
            return false;
        }

        if (!is_array($this->user_google))
        {
            $this->addError('user_google', 'Данные полученные от Google не свщяются массивом');

            return false;
        }

        // -----------------

        if (!isset($this->user_google['email']))
        {
            $this->addError('user_google', 'В данных от Google отсутствует нода email');

            return false;
        }

        if (!isset($this->user_google['name']))
        {
            $this->addError('user_google', 'В данных от Google отсутствует нода name');

            return false;
        }

        if (!isset($this->user_google['given_name']))
        {
            $this->addError('user_google', 'В данных от Google отсутствует нода given_name');

            return false;
        }

        if (!isset($this->user_google['locale']))
        {
            $this->user_google['locale'] = 'en';
        }

        if (!isset($this->user_google['family_name']))
        {
            $this->addError('user_google', 'В данных от Google отсутствует нода family_name');

            return false;
        }

        if (!isset($this->user_google['picture']))
        {
            $this->addError('user_google', 'В данных от Google отсутствует нода picture');

            return false;
        }

        // -----------------

        if (!is_string($this->user_google['email']))
        {
            $this->addError('user_google', 'В данных от Google нода email не является строкой');

            return false;
        }

        if (!is_string($this->user_google['name']))
        {
            $this->addError('user_google', 'В данных от Google нода name не является строкой');

            return false;
        }

        if (!is_string($this->user_google['given_name']))
        {
            $this->addError('user_google', 'В данных от Google нода given_name не является строкой');

            return false;
        }

        if (!is_string($this->user_google['locale']))
        {
            $this->addError('user_google', 'В данных от Google нода locale не является строкой');

            return false;
        }

        if (!is_string($this->user_google['family_name']))
        {
            $this->addError('user_google', 'В данных от Google нода family_name не является строкой');

            return false;
        }

        if (!is_string($this->user_google['picture']))
        {
            $this->addError('user_google', 'В данных от Google нода picture не является строкой');

            return false;
        }

        // -----------------

        if (!strlen($this->user_google['email']))
        {
            $this->addError('user_google', 'В данных от Google нода email пустая');

            return false;
        }

        if (!strlen($this->user_google['name']))
        {
            $this->addError('user_google', 'В данных от Google нода name пустая');

            return false;
        }

        if (!strlen($this->user_google['given_name']))
        {
            $this->addError('user_google', 'В данных от Google нода given_name пустая');

            return false;
        }

        if (!strlen($this->user_google['locale']))
        {
            $this->addError('user_google', 'В данных от Google нода locale пустая');

            return false;
        }

        if (!strlen($this->user_google['family_name']))
        {
            $this->addError('user_google', 'В данных от Google нода family_name пустая');

            return false;
        }

        if (!strlen($this->user_google['picture']))
        {
            $this->addError('user_google', 'В данных от Google нода picture пустая');

            return false;
        }

        // -----------------

        if (!filter_var($this->user_google['email'], FILTER_VALIDATE_EMAIL))
        {
            $this->addError('email', 'Указанный Email некорректный');

            return false;
        }

        $model_google_auth = new \xtetis\xuser\models\GoogleAuthModel([
            'email'       => $this->user_google['email'],
            'name'        => $this->user_google['name'],
            'given_name'  => $this->user_google['given_name'],
            'locale'      => $this->user_google['locale'],
            'family_name' => $this->user_google['family_name'],
            'picture'     => $this->user_google['picture'],
        ]);
        $model_google_auth->addGoogleAuthInfo();

        $email_array = explode('@', $this->user_google['email']);

        $this->login = $email_array[0];

        if (!mb_strlen($this->login))
        {
            $this->addError('login', 'Не заполнено имя пользователя');

            return false;
        }

        $this->email = $this->user_google['email'];

        // Проверка существования пользователя с таким Email
        $this->is_email_registered_result = \xtetis\xuser\models\SqlModel::xuserIsEmailRegistered(
            $this->email
        );

        // Если пользователь с таким Email уже зарегистрирован
        if ($this->is_email_registered_result['result'] > 0)
        {
            // Проверка существования пользователя по Email возвращает его ID. если найден
            $this->id = $this->is_email_registered_result['result'];

            // ПРоверка - активирован ли пользователь
            $this->is_user_activated_result = \xtetis\xuser\models\SqlModel::xuserIsUserActivated(
                $this->email
            );

            // Если не активирован
            if ($this->is_user_activated_result['result'] < 0)
            {
                // Автоматически автивируем её
                $this->active                   = 1;
                $this->insert_update_field_list = ['active'];

                // Обновляем запись
                $this->updateTableRecordById();
            }

            $this->getById();

            // Обнуляем хеш пароля, чтобы в случае доступа к сессии он был
            // недоступен для злоумышленников
            $this->pass = '';
            $this->result_sql = [];

            // Авторизируем пользователя (сохраняем данные о нем сессии)
            $this->saveModel();

            // Обновляет в БД дату последнего доступа
            $this->updateLastActivity();

            return true;

        }
        // Если такого пользователя нет

        else
        {
            // Генерируем пароль
            $this->pass = uniqid();

            // Зарегистрировал ли пользователь с таким логином
            $this->is_login_exists_result = \xtetis\xuser\models\SqlModel::xuserIsLoginExists(
                $this->login
            );

            // Если уже есть пользователь с таким логином
            if ($this->is_login_exists_result['result'] > 0)
            {
                // Генерируем рандомный логин
                $this->login .= '_' . uniqid();
            }

            // Добавляем пользователя в БД
            $this->register_user_result = \xtetis\xuser\models\SqlModel::xuserAddUser(
                $this->login,
                $this->email,
                $this->pass
            );

            // Если добавление в БД прошло неуспешно
            if ($this->register_user_result['result'] < 0)
            {
                $this->addError('common', $this->register_user_result['result_str']);

                return false;
            }

            $this->id = $this->register_user_result['result'];

            // Автоматически автивируем учетную запись
            $this->active                   = 1;
            $this->name                     = $this->user_google['given_name'];
            $this->insert_update_field_list = ['active', 'name'];

            // Обновляем запись в БД
            $this->updateTableRecordById();

            // Отправляем письмо о регистрации на сайте
            $this->sendRegisterGoogleEmail();

            // Обнуляем хеш пароля, чтобы в случае доступа к сессии он был
            // недоступен для злоумышленников
            $this->pass = '';
            $this->result_sql = [];

            // Авторизируем пользователя
            $this->saveModel();

            // Обновляет в БД дату последнего доступа
            $this->updateLastActivity();

            return true;
        }

        return false;
    }

    /**
     * Возвращает ссылку на логотип Gravatar текущего пользователя
     */
    public function getGravatarImgSrc()
    {
        if ($this->getErrors())
        {
            return false;
        }

        return \xtetis\xuser\models\GravatarModel::getGravatar($this->email)->url();
    }

    /**
     * Обновляет в БД дату последнего доступа
     */
    public function updateLastActivity()
    {
        if ($this->getErrors())
        {
            return false;
        }

        $this->last_access = 'now';

        $this->insert_update_field_list = ['last_access'];

        // Обновляем запись в БД
        $this->updateTableRecordById();

        return true;
    }

    /**
     * Возвращает дату последней активности форматированную
     */
    public function getLastAccessFormatted()
    {
        return date("d.m.Y H:i:s",strtotime($this->last_access));
    }

}
