<?php

/**
 * Выход из аккаунта
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

$response = [
    'result' => false,
];

if (\xtetis\xuser\Component::isLoggedIn())
{
    $response['errors']['common'] = 'Пользователь уже авторизирован';
    echo \xtetis\xengine\helpers\JsonHelper::arrayToJson($response);
    exit;
}

$email   = \xtetis\xengine\helpers\RequestHelper::post('email', 'str', '');
$pass    = \xtetis\xengine\helpers\RequestHelper::post('pass', 'str', '');
$captcha = \xtetis\xengine\helpers\RequestHelper::post('captcha', 'str', '');

$params = [
    'email'   => $email,
    'pass'    => $pass,
    'captcha' => $captcha,
];

$userModel                     = new \xtetis\xuser\models\UserModel($params);
$response['js_success']        = 'xuser.goToUrl();';
$response['js_fail']           = 'xcaptcha.reload();';
$response['data']['go_to_url'] = '/';
if ($userModel->loginByEmailPass())
{
    $response['result'] = true;

}
else
{
    $response['errors'] = $userModel->getErrors();
}

echo \xtetis\xengine\helpers\JsonHelper::arrayToJson($response);
exit;