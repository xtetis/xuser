<?php

/**
 * Выход из аккаунта
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

if (\xtetis\xuser\Component::isLoggedIn())
{
    header('Location: /');
    exit;
}

// Добавляет папку для поиска полей для xform
\xtetis\xform\Component::addFieldSearchFolder(
    \xtetis\xengine\App::getApp()->getComponent()->getComponentDirectory().'/views/field');

// Устанавливаем Title страницы, если не установлен другой
\xtetis\xengine\helpers\SeoHelper::setTitle('Авторизация');

// Устанавливаем Name страницы, если не установлен другой
\xtetis\xengine\helpers\SeoHelper::setPageName('Авторизация');

// Урл страницы для валидации формы авторизации
$url_login_validate_form = \xtetis\xuser\Component::makeUrl([
    'path'=>[
        \xtetis\xengine\App::getApp()->getAction(),
        'ajax_validate_login_form'
    ]
]);


// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage(
    [
        'url_login_validate_form'=>$url_login_validate_form
    ],
);
