<?php

/**
 * Выход из аккаунта
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

/*
$response = [
    'result' => false,
];

if (\xtetis\xuser\Component::isLoggedIn())
{
    $response['errors']['common'] = 'Пользователь уже авторизирован';
    echo \xtetis\xengine\helpers\JsonHelper::arrayToJson($response);
    exit;
}

$email   = \xtetis\xengine\helpers\RequestHelper::post('email', 'str', '');
$pass    = \xtetis\xengine\helpers\RequestHelper::post('pass', 'str', '');
$captcha = \xtetis\xengine\helpers\RequestHelper::post('captcha', 'str', '');

$params = [
    'email'   => $email,
    'pass'    => $pass,
    'captcha' => $captcha,
];

$userModel                     = new \xtetis\xuser\models\UserModel($params);
$response['js_success']        = 'xuser.goToUrl();';
$response['js_fail']           = 'xcaptcha.reload();';
$response['data']['go_to_url'] = '/';
if ($userModel->loginByEmailPass())
{
    $response['result'] = true;

}
else
{
    $response['errors'] = $userModel->getErrors();
}

echo \xtetis\xengine\helpers\JsonHelper::arrayToJson($response);
exit;

*/




$response = [
    'result' => false,
];

if (\xtetis\xuser\Component::isLoggedIn())
{
    return $response;
}

$id_user = \xtetis\xengine\helpers\RequestHelper::get('id', 'int', '');
$token   = \xtetis\xengine\helpers\RequestHelper::get('token', 'str', '');

$params = [
    'id' => $id_user,
    'token'   => $token,
];



$userModel = new \xtetis\xuser\models\UserModel($params);
if ($userModel->activateAccount())
{
    $login_url = \xtetis\xuser\Component::makeUrl([
        'path'=>[
            'login'
        ]
    ]);
    
    // Сохраняем в сессии сообщение для отображения на информационной странице
    $message = 'Ваша учетная запись успешно активирована. Теперь Вы можете <a href="'.$login_url.'">авторизироваться</a>';

    // Устанавливаем параметры для отображения страницы с сообщением для пользователя с единым урлом
    \xtetis\xpagemessage\Component::renderPageMessage([
        'message' => $message,
        'name'    => 'Учетная запись активирована',
        'title'   => 'Учетная запись активирована',
    ]);
}
else
{
    // Сохраняем в сессии сообщение для отображения на информационной странице
    $message = $userModel->getLastErrorMessage();

    // Устанавливаем параметры для отображения страницы с сообщением для пользователя с единым урлом
    \xtetis\xpagemessage\Component::renderPageMessage([
        'message' => $message,
        'name'    => 'Ошибки при активации',
        'title'   => 'Ошибки при активации',
    ]);
}

