<?php

    /**
     * Возвращает  отрендереную форму для повторной отправки активации
     */

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

if (\xtetis\xuser\Component::isLoggedIn())
{
    header('Location: /');
    exit;
}

// Добавляет папку для поиска полей для xform
\xtetis\xform\Component::addFieldSearchFolder(
    \xtetis\xengine\App::getApp()->getComponent()->getComponentDirectory().'/views/field');

// Устанавливаем Title страницы, если не установлен другой
\xtetis\xengine\helpers\SeoHelper::setTitle('Повторная отправка письма для активации',false);

// Устанавливаем Name страницы, если не установлен другой
\xtetis\xengine\helpers\SeoHelper::setPageName('Повторная активация',false);

// Урл страницы для валидации формы повторной отправки письма 
// для активации аккаунта
$url_validate_form = \xtetis\xuser\Component::makeUrl([
    'path'=>[
        \xtetis\xengine\App::getApp()->getAction(),
        'ajax_validate_resend_activate_form'
    ]
]);


// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage(
    [
        'url_validate_form'=>$url_validate_form
    ],
);
