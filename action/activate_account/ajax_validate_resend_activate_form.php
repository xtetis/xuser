<?php

/**
 * Валидирует форму повторной отправки письма для активации аккаунта
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

$response = [
    'result' => false,
];

if (\xtetis\xuser\Component::isLoggedIn())
{
    $response['errors']['common'] = 'Пользователь уже авторизирован';
    echo \xtetis\xengine\helpers\JsonHelper::arrayToJson($response);
    exit;
}

$email   = \xtetis\xengine\helpers\RequestHelper::post('email', 'str', '');
$captcha = \xtetis\xengine\helpers\RequestHelper::post('captcha', 'str', '');

$params = [
    'email'   => $email,
    'captcha' => $captcha,
];

$response['js_success']        = 'xuser.goToUrl();';
$response['js_fail']           = 'xcaptcha.reload();';
$response['data']['go_to_url'] = \xtetis\xpagemessage\Component::makeUrl();

$userModel = new \xtetis\xuser\models\UserModel($params);
if ($userModel->sendUserActivateEmail())
{
    $response['result'] = true;
}
else
{
    $response['errors'] = $userModel->getErrors();
}

echo \xtetis\xengine\helpers\JsonHelper::arrayToJson($response);
exit;
