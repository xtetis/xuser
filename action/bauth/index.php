<?php

/**
 * Автризация для бота
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

$id   = \xtetis\xengine\helpers\RequestHelper::get('id', 'int', 0);
$hash = \xtetis\xengine\helpers\RequestHelper::get('hash', 'str', '');



$model_bot = \xtetis\xuser\models\UserBotModel::generateModelById($id);
if (!$model_bot)
{
    \xtetis\xengine\helpers\LogHelper::customDie('Не найден пользователь');
}

$model_bot->auth_hash = $hash;

if (!$model_bot->loginBot())
{
    \xtetis\xengine\helpers\LogHelper::customDie($model_bot->getLastErrorMessage());
}




header("Location: /");
exit;
