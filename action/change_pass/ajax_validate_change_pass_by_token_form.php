<?php

/**
 * Валидирует форму отправки письма со ссылкой для восстановления пароля
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}






$response = [
    'result' => false,
];

if (\xtetis\xuser\Component::isLoggedIn())
{
    $response['errors']['common'] = 'Пользователь уже авторизирован';
    echo \xtetis\xengine\helpers\JsonHelper::arrayToJson($response);
    exit;
}

$token       = \xtetis\xengine\helpers\RequestHelper::post('token', 'str', '');
$pass        = \xtetis\xengine\helpers\RequestHelper::post('pass', 'str', '');
$pass_repeat = \xtetis\xengine\helpers\RequestHelper::post('pass_repeat', 'str', '');
$captcha     = \xtetis\xengine\helpers\RequestHelper::post('captcha', 'str', '');

$params = [
    'token'       => $token,
    'pass'        => $pass,
    'pass_repeat' => $pass_repeat,
    'captcha'     => $captcha,
];

$response['js_success'] = 'xuser.goToUrl();';
$response['js_fail'] = 'xcaptcha.reload();';
$response['data']['go_to_url'] = \xtetis\xpagemessage\Component::makeUrl();

$userModel = new \xtetis\xuser\models\UserModel($params);
if ($userModel->changeUserPassByToken())
{
    $response['result'] = true;
}
else
{
    $response['errors'] = $userModel->getErrors();
}


echo \xtetis\xengine\helpers\JsonHelper::arrayToJson($response);
exit;




