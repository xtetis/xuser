<?php


/**
 * Форма для изменения пароля по токену для неавторизированных пользователей  (возвращает отрендеренную станицу)
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

if (\xtetis\xuser\Component::isLoggedIn())
{
    header('Location: /');
    exit;
}


$token = \xtetis\xengine\helpers\RequestHelper::get('token', 'str', '');
$token = md5($token);

// Добавляет папку для поиска полей для xform
\xtetis\xform\Component::addFieldSearchFolder(
    \xtetis\xengine\App::getApp()->getComponent()->getComponentDirectory() . '/views/field'
);

// Устанавливаем Title страницы, если не установлен другой
\xtetis\xengine\helpers\SeoHelper::setTitle('Изменение пароля по токену', false);

// Устанавливаем Name страницы, если не установлен другой
\xtetis\xengine\helpers\SeoHelper::setPageName('Изменение пароля', false);



// Урл страницы для валидации формы регистрации
$url_validate_form = \xtetis\xuser\Component::makeUrl([
    'path' => [
        \xtetis\xengine\App::getApp()->getAction(),
        'ajax_validate_change_pass_by_token_form',
    ],
]);

// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage(
    [
        'url_validate_form' => $url_validate_form,
        'token'=>$token
    ],
);
