<?php

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

\xtetis\xuser\Config::validateGoogleAuthParams();

/**
 * Валидация ответа от Google при авторизации через oauth
 */

$post = print_r($_POST, true);

file_put_contents(LOG_DIRECTORY . '/callback_data_google.txt',
    'post = ' . $post . "\n\n" .
    ''
);
chmod(LOG_DIRECTORY . '/callback_data_google.txt', 0777);

$credential = \xtetis\xengine\helpers\RequestHelper::post('credential', 'str', '');
$client     = new Google\Client([
    'client_id' => GOOGLE_CLIENT_ID,
]);
$user_google = $client->verifyIdToken($credential);

$response = [
    'result' => false,
];
$response['post']        = $_POST;
$response['user_google'] = $user_google;

$model_user = new \xtetis\xuser\models\UserModel([
    'user_google' => $user_google,
]);
if ($model_user->loginByGoogleOauth())
{
    $response['result'] = true;
}
else
{
    $response['errors'] = $model_user->getErrors();
}



echo \xtetis\xengine\helpers\JsonHelper::arrayToJson($response);
exit;
