<?php

/**
 * Выход из аккаунта
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

\xtetis\xuser\models\UserModel::logout();

header('Location: /');
exit;
