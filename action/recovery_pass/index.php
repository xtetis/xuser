<?php

/**
 * Возвращает  отрендереную форму для запроса письма для восстановления пароля
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

if (\xtetis\xuser\Component::isLoggedIn())
{
    header('Location: /');
    exit;
}

// Добавляет папку для поиска полей для xform
\xtetis\xform\Component::addFieldSearchFolder(
    \xtetis\xengine\App::getApp()->getComponent()->getComponentDirectory().'/views/field');

// Устанавливаем Title страницы, если не установлен другой
\xtetis\xengine\helpers\SeoHelper::setTitle('Запрос письма для восстановления пароля',false);

// Устанавливаем Name страницы, если не установлен другой
\xtetis\xengine\helpers\SeoHelper::setPageName('Восстановление пароля',false);

// Урл страницы для валидации формы восстановления пароля
$url_validate_form = \xtetis\xuser\Component::makeUrl([
    'path'=>[
        \xtetis\xengine\App::getApp()->getAction(),
        'ajax_validate_recovery_pass_form'
    ]
]);


// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage(
    [
        'url_validate_form'=>$url_validate_form
    ],
);






