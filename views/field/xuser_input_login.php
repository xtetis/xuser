<?php
    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

?>

<div class=" mb-3">
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text"><i data-feather="user"></i></span>
        </div>
        <input <?=(isset($attributes['type'])?'type="'.$attributes['type'].'"':'type="text"')?>
               <?=(isset($attributes['name'])?'name="'.$attributes['name'].'"':'name="login"')?>
               <?=(isset($attributes['class'])?'class="'.$attributes['class'].'"':'class="form-control"')?>
               <?=(isset($attributes['placeholder'])?'placeholder="'.$attributes['placeholder'].'"':'placeholder="Пользователь"')?>
               value="<?=$value?>">
    </div>
    <div class="error_form__login form_error_item"></div>
</div>