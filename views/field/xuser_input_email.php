<?php
    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

?>
<div class=" mb-3">
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text"><i data-feather="mail"></i></span>
        </div>
        <input type="email"
               name="email"
               class="form-control"
               placeholder="Email">
    </div>
    <div class="error_form__email form_error_item"></div>
</div>