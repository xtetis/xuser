<?php
    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

?>
<div class=" mb-4">
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text"><i data-feather="lock"></i></span>
        </div>
        <input type="password"
               name="pass_repeat"
               class="form-control"
               placeholder="Повторите пароль">
    </div>
    <div class="error_form__pass_repeat form_error_item"></div>
</div>