<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    // Добавляем файл JS для обработки формы
    \xtetis\xengine\helpers\StatResHelper::addTemplateJs(__DIR__ . '/../../web/js/xuser.js');
?>

<script src="https://accounts.google.com/gsi/client" async defer></script>
    <div id="g_id_onload"
         data-client_id="<?=GOOGLE_CLIENT_ID?>"
         data-callback="handleCredentialResponse"
         >
    </div>
    <div class="g_id_signin" data-type="icon"></div>
    <input type="hidden" id="url_google_auth_validate" value="<?=$url_google_auth_validate?>">
