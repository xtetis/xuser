<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    // Добавляем файл JS для обработки формы
    \xtetis\xengine\helpers\StatResHelper::addTemplateJs(__DIR__ . '/../../web/js/xuser.js');


?>
<div class="text-center">
    <?=\xtetis\xform\Component::renderOnlyFormStart([
    'url_validate' => $url_login_validate_form,
    'form_type'    => 'ajax',
    ]);?>
    <h4 class="mb-3 f-w-400">Авторизация</h4>
    <?=\xtetis\xform\Component::renderField(['template'=>'xuser_input_email'])?>
    <?=\xtetis\xform\Component::renderField(['template'=>'xuser_input_pass'])?>
    <?=\xtetis\xcaptcha\Component::renderCaptchaBlock();?>
    <button type="submit"
            class="btn btn-block btn-primary mb-4">Войти</button>
    <?=\xtetis\xform\Component::renderFormEnd();?>
</div>
