# xtetis\xuser
## Модуль пользователей для движка xEngine

Powered by xTetis

Создает страницы:
 - Авторизация (../login)
 - Регистрация (../register)
 - Восстановить пароль (../send_recovery_pass)
и другие сервисные страницы модуля "Пользователь"
---
## Установка модуля

 - Если Вы используете xEngine (https://bitbucket.org/xtetis/xengine/), то он содержит в ноде require файла composer.json, потому будет установлен автоматически
```sh
"xtetis/xuser": "dev-master",
```
 - После установки модуля необходимо прописать следующие константы

| Константа | Тип     | Пример                | Описание                |
| :-------- | :------- | :------------------------- | :------------------------- |
| `COMPONENT_URL_LIST` | `строка, сериализованный массив` | define("COMPONENT_URL_LIST", serialize(['user' => 'xtetis\xuser'])); | Список урлом модулей xEngine |
| `DEFAULT_LAYOUT` | `строка` | define("DEFAULT_LAYOUT", 'page'); | Шаблон по-умолчанию |
 
 - в шаблоне прописать урлы для

```php

    // Урл регистрационной формы (возвращает отрендеренную станицу)
    $url_register_form = \xtetis\xuser\Component::getModuleUrl('register');

    // Урл формы авторизации (возвращает отрендеренную станицу)
    $url_login_form = \xtetis\xuser\Component::getModuleUrl('login');

    // Урл формы для отправки письма для восстановления пароля  (возвращает отрендеренную станицу)
    $url_send_recovery_pass_form = \xtetis\xuser\Component::getModuleUrl('send_recovery_pass');

```
 - В шаблоне должен присутствовать вызов (для вывода зарегистрированных JS компонента) ниже определения jQuery 
```php
<?=\xtetis\xengine\helpers\StatResHelper::getTemplateJsList()?>
```
 - Методы
```php
//Авторизирован ли пользователь
\xtetis\xuser\Component::isLoggedIn():bool

//Получение урла события модуля
\xtetis\xuser\Component::getModuleUrl(
        string $module_action = '',
        array  $query = [],
        bool   $with_host = false
    ):string
    
// Запуск события модуля
\xtetis\xuser\Component::runModuleAction(
        $module_action = '',
        $action_params = []
    )
```



## Настройка авторизации через Google

Для вывода кнопки авторизации

```php

<?=\xtetis\xuser\Component::getButtonGoogleAuth()?>

```



## Обратная связь

Для связи с автором автором
 - skype: xtetis
 - telegram: @xtetis
